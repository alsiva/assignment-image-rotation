#include "../include/image.h"
#include "../include/transition_rotate.h"
#include <malloc.h>
#include <stdint.h>

struct image* rotate_image(const struct image* src) {
    struct image* dest = image_create(src->height, src->width); //swap width/height
    if (dest == NULL) {
        return dest;
    }

    for (uint32_t row = 0; row < src->height; row++) {
        uint32_t rotated_column = row;

        for (uint32_t column = 0; column < src->width; column++) {
            uint32_t rotated_row = src->width - 1 - column;

            *get_pixel(dest, rotated_row, rotated_column) = *get_pixel(src, row, column);
        }
    }

    return dest;
}
