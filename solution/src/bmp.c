#include "../include/bmp.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

static const uint16_t BMP_TYPE = 0x4D42;

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};



uint32_t calculate_padding(uint32_t row_byte_count) {
    return (4 - row_byte_count % 4) % 4;
}

enum read_status from_bmp(FILE* in, struct image** image) {
    struct bmp_header header;

    fread(&header, sizeof(header), 1, in);
    fseek(in, header.bOffBits, SEEK_SET);

    if (ferror(in)) {
        return ERROR;
    }

    if (header.bfType != BMP_TYPE) {
        return IMAGE_NOT_SUPPORTED;
    }

    *image = image_create(header.biWidth, header.biHeight);
    if (image == NULL) {
        return ERROR;
    }
    
     if (header.biBitCount != 24) {
        return IMAGE_NOT_SUPPORTED;
     }
    
    uint32_t row_byte_count = header.biWidth * sizeof(struct pixel);
    uint32_t padding = calculate_padding(row_byte_count);

    for (uint32_t i = 0; i < header.biHeight; i++) {
        uint32_t row = header.biHeight - 1 - i;
    
        for (uint32_t j = 0; j < header.biWidth; j++) {
            struct pixel pixel;
            fread(&pixel, sizeof(pixel), 1, in);

            *get_pixel(*image, row, j) = (struct pixel) {
                pixel.b, pixel.g, pixel.r
            };
        }
        
        if (padding != 0) {
            fseek(in, padding, SEEK_CUR);
        }
    }

    if (ferror(in)) {
        return ERROR;
    }

    return OK;
}

enum write_status to_bmp(FILE* out, const struct image* image) {
    uint32_t row_byte_count = image->width * sizeof(struct pixel);
    uint32_t padding = calculate_padding(row_byte_count);

    const uint32_t raw_image_size = (row_byte_count + padding) * image->height;

    struct bmp_header header = {
        .bfType = BMP_TYPE,
        .bfileSize = sizeof(struct bmp_header) + raw_image_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = raw_image_size,
        .biXPelsPerMeter = 1000,
        .biYPelsPerMeter = 1000,
        .biClrUsed = 256 * 256 * 256,
        .biClrImportant = 0
    };

    fwrite(&header, sizeof(struct bmp_header), 1, out);

    if (ferror(out)) {
        return WRITE_ERROR;
    }


    for (uint32_t i = 0; i < image->height; i++) {
        uint32_t row = image->height - 1 - i;
        uint32_t start_of_row = row * image->width;

        for (uint32_t j = 0; j < image->width; j++) {
            struct pixel pixel = image->pixels[start_of_row + j];
            fwrite(&pixel, sizeof(struct pixel), 1, out);
        }

        if (padding != 0) {
            fwrite(image->pixels, padding, 1, out);
        }
    }

    if (ferror(out)) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
