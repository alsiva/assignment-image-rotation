#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/transition_rotate.h"


int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "You should pass input file and output file as program arguments");
        return 1;
    }

    char* input_file_name = argv[1];
    char* output_file_name = argv[2];

    FILE* input_file = fopen(input_file_name, "rb");

    if (input_file == NULL) {
        if (errno == ENOENT) {
            fprintf(stderr, "%s: no such file\n", input_file_name);
            return 1;
        } else {
            fprintf(stderr, "Can't open %s\n", input_file_name);
            return 2;
        }
    }

    struct image* image;
    enum read_status read_status = from_bmp(input_file, &image);
    if (fclose(input_file) != 0) {
        fprintf(stderr, "failed to close input file");
    }

    if (read_status != OK) {
        fprintf(stderr, "Error reading image");
        return 3;
    }

    struct image* rotated_image = rotate_image(image);
    if (rotated_image == NULL) {
        fprintf(stderr, "failed to create rotated image");
        return 4;
    }

    image_free(image);

    FILE* output_file = fopen(output_file_name, "wb");

    uint32_t result;
    if (output_file == NULL) {
        if (errno == ENOENT) {
            fprintf(stderr, "%s: no such file\n", output_file_name);
            result = 5;
        } else {
            fprintf(stderr, "Can't open %s\n", output_file_name);
            result = 6;
        }
    } else {
        enum write_status write_status = to_bmp(output_file, rotated_image);
        if (fclose(output_file) != 0) {
            fprintf(stderr, "failed to close output file");
        }
        result = write_status == WRITE_OK ? 0 : 7;
    }
    
    image_free(rotated_image);
    
    return result;
}
