#include "../include/image.h"
#include <malloc.h>
#include <stdint.h>

struct pixel* get_pixel(const struct image* image, const uint32_t row, const uint32_t column) {
    return image->pixels + row * image->width + column;
}

void image_free(struct image* image) {
    if (image->pixels != NULL) {
        free(image->pixels);
        image->pixels = NULL;
        image->width = 0;
        image->height = 0;
    }
    free(image);
}

struct image* image_create(const uint32_t width, const uint32_t height) {
    const uint32_t size = sizeof(struct pixel) * width * height;
    struct pixel* pixels = malloc(size);

    if (pixels == NULL && size != 0) {
        return NULL;
    }
    
    struct image* image = malloc(sizeof(struct image));
    if (image == NULL) {
        free(pixels);
        return NULL;
    }

    image->width = width;
    image->height = height;
    image->pixels = pixels;

    return image;
}
