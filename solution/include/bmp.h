#ifndef MY_PROJECT_BMP_HANDLER_H
#define MY_PROJECT_BMP_HANDLER_H

#include "image.h"
#include <stdbool.h>
#include <stdio.h>


enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum read_status {
    OK,
    ERROR,
    IMAGE_NOT_SUPPORTED,
};

enum read_status from_bmp(FILE* in, struct image** image);

enum write_status to_bmp(FILE* out, const struct image* image);

#endif
