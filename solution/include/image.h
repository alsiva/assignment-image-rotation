#ifndef MY_PROJECT_IMAGE_H
#define MY_PROJECT_IMAGE_H

#include <inttypes.h>

struct image {
    uint32_t width, height;
    struct pixel* pixels;
};

struct pixel {
    uint8_t b, g, r;
};

struct pixel* get_pixel(const struct image* image, const uint32_t row, const uint32_t column);

void image_free(struct image* image);
struct image* image_create(const uint32_t width,  const uint32_t height);

#endif
