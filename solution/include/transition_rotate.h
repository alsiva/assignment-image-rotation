#ifndef MY_PROJECT_TRANSITION_ROTATE_H
#define MY_PROJECT_TRANSITION_ROTATE_H

#include "image.h"

struct image* rotate_image(const struct image* src);

#endif
